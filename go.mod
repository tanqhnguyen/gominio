module gitlab.com/tanqhnguyen/gominio

go 1.14

require (
	github.com/minio/minio-go/v6 v6.0.56
	github.com/stretchr/testify v1.6.1
	github.com/tanqhnguyen/envconfig v1.5.4
	gitlab.com/tanqhnguyen/gologger v0.1.2
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859 // indirect
)
