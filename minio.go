package gominio

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"mime"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/minio/minio-go/v6"
	"github.com/tanqhnguyen/envconfig"
	"gitlab.com/tanqhnguyen/gologger"
)

// FileUploader interface defines common methods to upload files
type FileUploader interface {
	UploadFile(name string, content []byte) error
	UploadFromURL(path string) error
	FileExists(name string) (bool, error)
	GenerateDownloadLink(name string, duration time.Duration) (string, error)
}

// MinioConfig is the configuration of a minio server
type MinioConfig struct {
	AccessKey string `envconfig:"access_key" required:"true" file_content:"true"`
	SecretKey string `envconfig:"secret_key" required:"true" file_content:"true"`
	Host      string `envconfig:"host" default:"127.0.0.1"`
	Port      int    `envconfig:"port" default:"9000"`
	Scheme    string `envconfig:"scheme" default:"http"`
}

// LoadMinioConfigFromEnvironment returns the minio config as set in the environment variable
func LoadMinioConfigFromEnvironment() (*MinioConfig, error) {
	var fromEnv MinioConfig
	err := envconfig.Process("MINIO", &fromEnv)

	if err != nil {
		return nil, err
	}
	return &fromEnv, nil
}

// NewClientFromEnvironment returns a new minio client based on env variables
func NewClientFromEnvironment(bucketName string) *MinioClient {
	config, err := LoadMinioConfigFromEnvironment()
	if err != nil {
		panic(err)
	}

	secure := false
	if config.Scheme == "https" {
		secure = true
	}

	address := fmt.Sprintf("%s:%d", config.Host, config.Port)
	if config.Port == 80 || config.Port == 443 {
		address = config.Host
	}

	minioClient, err := minio.New(address, config.AccessKey, config.SecretKey, secure)
	if err != nil {
		panic(err)
	}

	logger := gologger.NewLogrusLoggerFromEnvironment()

	location := "eu-west-1"

	err = minioClient.MakeBucket(bucketName, location)
	if err != nil {
		exists, errBucketExists := minioClient.BucketExists(bucketName)
		if errBucketExists == nil && exists {
			logger.Field("bucket", bucketName).Info("bucket already exists")
		} else {
			panic(err)
		}
	} else {
		logger.Field("bucket", bucketName).Info("created a new bucket")
	}

	return &MinioClient{
		minioClient,
		bucketName,
		location,
		logger,
	}
}

// MinioClient contains logic to interact with a minio server
type MinioClient struct {
	client     *minio.Client
	bucketName string
	location   string
	logger     gologger.Logger
}

// UploadFile sends a file to minio
func (m *MinioClient) UploadFile(name string, content []byte) error {
	parts := strings.Split(name, ".")
	contentType := mime.TypeByExtension("." + parts[len(parts)-1])

	m.logger.Fields(map[string]interface{}{
		"name":        name,
		"contentType": contentType,
	}).Debug("storing file")

	_, err := m.client.PutObject(
		m.bucketName,
		name,
		bytes.NewReader(content),
		int64(len(content)),
		minio.PutObjectOptions{ContentType: contentType},
	)

	return err
}

// UploadFromURL uploads from an url
func (m *MinioClient) UploadFromURL(path string) error {
	urlObj, err := url.Parse(path)
	if err != nil {
		return err
	}

	res, err := http.Get(path)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)

	paths := strings.Split(urlObj.Path, "/")
	fileName := paths[len(paths)-1]
	m.logger.
		Field("path", urlObj.Path).
		Field("fileName", fileName).
		Debug("storing file from URL")

	// Path has leading trail
	return m.UploadFile(fileName, body)
}

// FileExists checks if file exists
func (m *MinioClient) FileExists(name string) (bool, error) {
	objInfo, err := m.client.StatObject(m.bucketName, name, minio.StatObjectOptions{})

	if err != nil && err.(minio.ErrorResponse).Code == "NoSuchKey" {
		return false, nil
	}

	if err != nil {
		return false, err
	}

	return objInfo.Size > 0, nil
}

const maxDuration = 24 * 7 * time.Hour

// GenerateDownloadLink generates a valid download link for the file
// `duration` is capped at 7 days
func (m *MinioClient) GenerateDownloadLink(name string, duration time.Duration) (string, error) {
	if duration > maxDuration {
		duration = maxDuration
	}
	u, err := m.client.PresignedGetObject(m.bucketName, name, duration, url.Values{})

	if err != nil {
		return "", err
	}

	return u.String(), nil
}
